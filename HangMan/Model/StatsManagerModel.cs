﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HangMan.Model
{
	[Serializable]
	public class StatsManagerModel
	{
		public Dictionary<string, StatsModel> users = new Dictionary<string, StatsModel>();

		public void logGame(string username, string domainName, bool victory)
		{
			if(!users.ContainsKey(username))
			{
				StatsModel statsModel = new StatsModel();
				statsModel.name = username;
				statsModel.totalGames++;

				if (victory)
				{
					statsModel.wonGames++;
					statsModel.wonGamesByDomain[domainName]++;
				}

				users.Add(username, statsModel);
			}
			else
			{
				users[username].totalGames++;

				if (victory)
				{
					users[username].wonGames++;
					users[username].wonGamesByDomain[domainName]++;
				}
			}
		}

		public void removeUser(string username)
		{
			if (users.ContainsKey(username))
			{
				users.Remove(username);
			}
		}

		public void Serialize()
		{
			SerializationManager.Serialize(this);
		}
	}
}
