﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HangMan.Model
{
	[Serializable]
	public class UsersModel : ObservableCollection<UserModel>
	{
		public bool userExists(string username)
		{
			foreach (UserModel user in this)
			{
				if (user.name == username)
				{
					return true;
				}
			}

			return false;
		}

		public void deleteUser(string username)
		{
			this.Remove(this.Items.Where(d => d.name == username).ElementAt(0));
		}
	}
}
