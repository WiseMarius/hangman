﻿using HangMan.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HangMan.Model
{
	[Serializable]
	public class StatsModel
	{
		public readonly DomainsViewModel domainsViewModel = new DomainsViewModel();
		public Dictionary<string, int> wonGamesByDomain = new Dictionary<string, int>();

		public string name { get; set; }
		public int totalGames { get; set; }
		public int wonGames { get; set; }


		public StatsModel()
		{
			totalGames = 0;
			wonGames = 0;

			for(int index = 0; index < domainsViewModel.domainsName.Length - 1; index++)
			{
				wonGamesByDomain.Add(domainsViewModel.domainsName[index], 0);
			}
		}
	}
}
