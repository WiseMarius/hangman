﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HangMan.Model
{
	[Serializable]
	public class DomainsModel
	{
		private static readonly string[] animals = { "fox", "bat", "bear", "leopard", "lion", "bunny" };
		private static readonly string[] countries = { "Romania", "Germany", "Serbia", "Bulgary" };
		private static readonly string[] fruits = { "apple", "pears", "strawberries", "cherries" };
		private static readonly string[] empty = { "" };

		public string[] getDomain(byte index)
		{
			string[] allDomains = new string[animals.Length + countries.Length + fruits.Length];
			allDomains.Concat(animals).Concat(countries).Concat(fruits);
			animals.CopyTo(allDomains, 0);
			countries.CopyTo(allDomains, animals.Length);
			fruits.CopyTo(allDomains, animals.Length + countries.Length);

			switch (index)
			{
				case 0:
					{
						return countries;
					}
				case 1:
					{
						return animals;
					}
				case 2:
					{
						return fruits;
					}
				case 3:
					{
						return allDomains;
					}
				case 4:
					{
						return empty;
					}
				default: return allDomains;
			}
		}
	}
}
