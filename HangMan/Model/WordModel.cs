﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HangMan.Model
{
	public class WordModel
	{
		private DomainsModel domainsModels = new DomainsModel();
		public readonly string word;

		public WordModel(byte domainIndex)
		{
			string[] words = domainsModels.getDomain(domainIndex);
			int randomNum = new Random().Next(words.Length);
			word = words[randomNum];
		}
	}
}
