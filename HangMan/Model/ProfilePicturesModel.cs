﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HangMan.Model
{
	public class ProfilePicturesModel
	{
		public readonly string[] pictures =
		{
			AppDomain.CurrentDomain.BaseDirectory + @"ProfilePictures\1.jpg",
			AppDomain.CurrentDomain.BaseDirectory + @"ProfilePictures\2.png",
			AppDomain.CurrentDomain.BaseDirectory + @"ProfilePictures\3.jpg",
			AppDomain.CurrentDomain.BaseDirectory + @"ProfilePictures\4.jpg",
			AppDomain.CurrentDomain.BaseDirectory + @"ProfilePictures\5.jpg",
			AppDomain.CurrentDomain.BaseDirectory + @"ProfilePictures\6.jpg",
			AppDomain.CurrentDomain.BaseDirectory + @"ProfilePictures\7.jpg"
		};

		public short index { get; set; }

		public ProfilePicturesModel()
		{
			index = 0;
		}
	}
}
