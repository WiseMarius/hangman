﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HangMan.Model
{
	[Serializable]
	public class UserModel
	{
		public UserModel()
		{

		}
		public UserModel(string name, short avatar, short games, short gamesWon)
		{
			this.name = name;
			this.avatar = avatar;
			this.games = games;
			this.gamesWon = gamesWon;
		}

		public UserModel(string name)
		{
			this.name = name;
			this.avatar = 0;
			this.games = 0;
			this.gamesWon = 0;
		}

		public string name { get; set; }
		public short avatar { get; set; }
		public short games { get; set; }
		public short gamesWon { get; set; }

		public override string ToString()
		{
			return this.name;
		}
	}
}
