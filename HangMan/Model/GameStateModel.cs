﻿using HangMan.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HangMan
{
	[Serializable]
	public class GameStateModel
	{
		public DomainsViewModel domainsViewModel = new DomainsViewModel();
		public TimerViewModel timerViewModel = new TimerViewModel();
		public StateImageViewModel stateImageViewModel = new StateImageViewModel();
		public WordViewModel wordViewModel;
		public bool isGameSaved = false;

		public readonly int maxRounds = 5;
	}
}
