﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HangMan.Model
{
	public class StateImageModel
	{
		public readonly string[] stateImages =
		{
			@"/Resources/man0.png",
			@"/Resources/man1.png",
			@"/Resources/man2.png",
			@"/Resources/man3.png",
			@"/Resources/man4.png",
			@"/Resources/man5.png",
			@"/Resources/man6.png",
			@"/Resources/man7.png",
		};

		public short index { get; set; } = 0;
	}
}
