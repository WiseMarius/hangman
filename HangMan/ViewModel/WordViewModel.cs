﻿using HangMan.Event;
using HangMan.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HangMan.ViewModel
{
	public class WordViewModel : INotifyPropertyChanged
	{
		public event MyEventHandler OnWordGuessed;
		public event MyEventHandler OnWrongLetter;

		private WordModel wordModel;

		public event PropertyChangedEventHandler PropertyChanged;

		private string _wordToShow;
		public string wordToShow
		{
			get
			{
				return _wordToShow;
			}
			set
			{
				if(_wordToShow != value)
				{
					_wordToShow = value;

					NotifyPropertyChanged("wordToShow");
				}
			}
		}
		public string word;

		public WordViewModel()
		{
			wordToShow = "Please start game";
		}

		public WordViewModel(byte domainIndex)
		{
			wordModel = new WordModel(domainIndex);

			word = wordModel.word;
			wordToShow = word;
			Console.WriteLine(word);
			addLetter("");
		}

		public void NotifyPropertyChanged(string propertyName)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		public bool addLetter(string letterToAdd)
		{
			if(letterToAdd == "")
			{
				StringBuilder tempWord = new StringBuilder(word);

				for(int index = 0; index < word.Length; index++)
				{
					if((wordToShow[index] >= 65 && wordToShow[index] <= 90) || (wordToShow[index] >= 97 && wordToShow[index] <= 122))
					{
						tempWord[index] = '_';
					}
				}

				wordToShow = tempWord.ToString();

				return true;
			}
			else
			{
				if (!wordToShow.Contains(letterToAdd) && (word.IndexOf(letterToAdd, StringComparison.OrdinalIgnoreCase) >= 0))
				{
					StringBuilder tempWord = new StringBuilder(wordToShow);

					for (int index = 0; index < tempWord.Length; index++)
					{
						if (word[index] == letterToAdd[0] || word[index] == letterToAdd[0] + 32)
						{
							tempWord[index] = letterToAdd[0];
						}
					}

					wordToShow = tempWord.ToString();
					checkWord();

					return true;
				}
			}

			if (OnWrongLetter != null)
			{
				OnWrongLetter(this, new MyEventArgs("Wrong letter!"));
			}

			return false;
		}

		private void checkWord()
		{
			if(String.Equals(wordToShow, word, StringComparison.OrdinalIgnoreCase))
			{
				if (OnWordGuessed != null)
				{
					OnWordGuessed(this, new MyEventArgs("Word guessed!"));
				}
			}
		}
	}
}
