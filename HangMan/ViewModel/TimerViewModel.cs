﻿using HangMan.Model;
using HangMan.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Threading;
using HangMan.Event;

namespace HangMan.ViewModel
{
	public class TimerViewModel : INotifyPropertyChanged
	{
		public event MyEventHandler OnTimeOut;

		private TimerModel timerModel = new TimerModel();
		private DispatcherTimer timer = new DispatcherTimer();
		public event PropertyChangedEventHandler PropertyChanged;
		private short _currentTime;
		public short currentTime
		{
			get
			{
				return _currentTime;
			}
			set
			{
				if(_currentTime != value)
				{
					_currentTime = value;

					NotifyPropertyChanged("currentTime");
					NotifyPropertyChanged("timeMessage");
				}
			}
		}
		private string _timeMessage;
		public string timeMessage
		{
			get
			{
				return _timeMessage;
			}
			set
			{
				if(_timeMessage != value)
				{
					_timeMessage = value;

					NotifyPropertyChanged("timeMessage");
				}
			}
		}

		public TimerViewModel()
		{
			currentTime = timerModel.timelimit;
			timer.Interval = new TimeSpan(0, 0, 1);
			timer.Tick += Timer_Tick;
			timeMessage = "Timeleft: ";
		}

		private void Timer_Tick(object sender, EventArgs e)
		{
			if (currentTime <= 0)
			{
				timer.Stop();

				if(OnTimeOut != null)
				{
					OnTimeOut(this, new MyEventArgs("Time out"));
				}
			}
			else
			{
				currentTime--;

				timeMessage = string.Format("{0}00:{1}{2}", timerModel.timerPrefix, currentTime / 60, currentTime % 60);
			}
		}

		public void start()
		{
			resetTimer();

			timer.Start();
		}

		public void stop()
		{
			timer.Stop();
		}

		public void resume()
		{
			timer.Start();
		}

		public void resetTimer()
		{
			currentTime = timerModel.timelimit;
		}

		public void NotifyPropertyChanged(string propertyName)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
}
