﻿using HangMan;
using HangMan.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HangMan.ViewModel
{
	[Serializable]
	public class UsersViewModel
	{
		public UsersModel users { get; set; }
		public UserModel selectedUser { get; set; }

		public UsersViewModel()
		{
			users = new UsersModel();
			selectedUser = new UserModel();
		}
	}
}
