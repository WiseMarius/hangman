﻿using HangMan.Event;
using HangMan.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HangMan.ViewModel
{
	public class StateImageViewModel : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		public StateImageModel stateImageModel = new StateImageModel();
		private string _getCurrentStateImage;
		public string getCurrentStateImage
		{
			get
			{
				return _getCurrentStateImage;
			}
			set
			{
				if(_getCurrentStateImage != value)
				{
					_getCurrentStateImage = value;

					NotifyPropertyChanged("getCurrentStateImage");
				}
			}
		}

		public StateImageViewModel()
		{
			getCurrentStateImage = stateImageModel.stateImages[stateImageModel.index];
		}

		public void nextState()
		{
			if (stateImageModel.index < 6)
			{
				getCurrentStateImage = stateImageModel.stateImages[++stateImageModel.index];
			}
		}

		public void resetState()
		{
			stateImageModel.index = 0;
			getCurrentStateImage = stateImageModel.stateImages[0];
		}

		public void NotifyPropertyChanged(string propertyName)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
}
