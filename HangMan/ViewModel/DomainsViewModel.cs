﻿using HangMan.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HangMan.ViewModel
{
	[Serializable]
	public class DomainsViewModel : INotifyPropertyChanged
	{
		public readonly DomainsModel domainsModel = new DomainsModel();
		private byte _selectedIndex = 4;
		public byte selectedIndex
		{
			get
			{
				return _selectedIndex;
			}
			set
			{
				if (_selectedIndex != value)
				{
					_selectedIndex = value;
					_selectedDomain = domainsName[_selectedIndex];

					NotifyPropertyChanged("selectedIndex");
					NotifyPropertyChanged("selectedDomain");
				}
			}
		}
		public readonly string[] domainsName = new string[5]
		{
			"Countries",
			"Animals",
			"Fruits",
			"All domains",
			"Domain not selected"
		};
		private string _selectedDomain;
		public string selectedDomain
		{
			get
			{
				return _selectedDomain;
			}
			set
			{
				if (_selectedDomain != value)
				{
					_selectedDomain = value;
					NotifyPropertyChanged("selectedDomain");
				}
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		public DomainsViewModel()
		{
			selectedIndex = 4;
			selectedDomain = domainsName[selectedIndex];
		}

		public void NotifyPropertyChanged(string propertyName)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
}
