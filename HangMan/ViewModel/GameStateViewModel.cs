﻿using HangMan.Event;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Xml.Serialization;

namespace HangMan.ViewModel
{
	public class GameStateViewModel : INotifyPropertyChanged
	{
		[field:NonSerialized]
		public event PropertyChangedEventHandler PropertyChanged;
		[field: NonSerialized]
		public event MyEventHandler OnGameWon;

		public GameStateModel gameStateModel = new GameStateModel();

		[XmlIgnoreAttribute]
		public Button[] buttonsWrongLetters = new Button[7];
		[XmlIgnoreAttribute]
		public Button[] buttonsLetters = new Button[27];

		public string wrongLetters;
		public string triedLetters;

		private readonly string roundMessagePrefix = "Round: ";
		public void NotifyPropertyChanged(string propertyName)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		private short _round;
		public short round
		{
			get
			{
				return _round;
			}
			set
			{
				if (_round != value)
				{
					_round = value;
					_roundMessage = roundMessage;

					NotifyPropertyChanged("round");
					NotifyPropertyChanged("roundMessage");
				}
			}
		}
		private string _roundMessage;
		public string roundMessage
		{
			get
			{
				return _roundMessage;
			}
			set
			{
				if (_roundMessage != value)
				{
					_roundMessage = value;

					NotifyPropertyChanged("roundMessage");
				}
			}
		}

		public bool gameActive { get; set; } = false;

		public void nextRound()
		{
			round++;

			if(round > gameStateModel.maxRounds)
			{
				if(OnGameWon != null)
				{
					OnGameWon(this, new MyEventArgs("You won!"));
				}
			}

			roundMessage = roundMessagePrefix + round.ToString();
		}

		public void restart()
		{

			round = 1;
			roundMessage = roundMessagePrefix + round.ToString();

			start();
		}

		public GameStateViewModel()
		{
			round = 1;

			roundMessage = roundMessagePrefix + round.ToString();
		}

		public void start()
		{
			gameStateModel.timerViewModel.start();
			gameActive = true;
		}

		public void stop()
		{
			if (gameActive)
			{
				gameStateModel.timerViewModel.stop();
				gameActive = false;
			}
		}

		public void resume()
		{
			if(!gameActive)
			{
				gameStateModel.timerViewModel.resume();
				gameActive = true;
			}
		}
	}
}
