﻿using HangMan.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HangMan.ViewModel
{
	public class ProfilePicturesViewModel : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;
		public ProfilePicturesModel profilePcitures = new ProfilePicturesModel();
		private string _currentPicture;
		public string currentPicture
		{
			get
			{
				return _currentPicture;
			}
			set
			{
				if(_currentPicture != value)
				{
					_currentPicture = value;
					NotifyPropertyChanged("currentPicture");
				}
			}
		}

		public ProfilePicturesViewModel()
		{
			currentPicture = profilePcitures.pictures[profilePcitures.index];
		}

		public void NotifyPropertyChanged(string propertyName)
		{
			if(PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		public void nextProfilePictureIndex()
		{
			profilePcitures.index += 1;

			if (profilePcitures.index > 6)
			{
				profilePcitures.index = 0;
			}

			currentPicture = profilePcitures.pictures[profilePcitures.index];
		}

		public void previousProfilePictureIndex()
		{
			profilePcitures.index -= 1;

			if(profilePcitures.index < 0)
			{
				profilePcitures.index = 6;
			}

			currentPicture = profilePcitures.pictures[profilePcitures.index];
		}
	}
}
