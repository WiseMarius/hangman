﻿using HangMan.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HangMan.ViewModel
{
	public struct TableData
	{
		public string name { get; set; }
		public int totalGames { get; set; }
		public int wonGames { get; set; }
		public int wonGamesAnimals { get; set; }
		public int wonGamesCountries { get; set; }
		public int wonGamesFruits { get; set; }
		public int wonGamesAllDomains { get; set; }

		public TableData(string pName, int pTotalGames, int pWonGames, int pWonGamesAnimals, int pWonGamesCountries, int pWonGamesFruits, int pWonGamesAlldomains)
		{
			this.name = pName;
			this.totalGames = pTotalGames;
			this.wonGames = pWonGames;
			this.wonGamesAnimals = pWonGamesAnimals;
			this.wonGamesCountries = pWonGamesCountries;
			this.wonGamesFruits = pWonGamesFruits;
			this.wonGamesAllDomains = pWonGamesAlldomains;
		}
	}
	public class StatsManagerViewModel
	{
		public readonly StatsManagerModel statsManagerModel;
		public ObservableCollection<TableData> tableData { get; set; } = new ObservableCollection<TableData>();

		public StatsManagerViewModel()
		{
			statsManagerModel = SerializationManager.DeserializeStats();

			foreach (var user in statsManagerModel.users)
			{
				tableData.Add(new TableData(user.Key, user.Value.totalGames, user.Value.wonGames, user.Value.wonGamesByDomain["Animals"], user.Value.wonGamesByDomain["Countries"], user.Value.wonGamesByDomain["Fruits"], user.Value.wonGamesByDomain["All domains"]));
			}
		}
	}
}
