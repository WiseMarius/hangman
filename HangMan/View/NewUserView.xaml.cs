﻿using HangMan.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HangMan.View
{
	/// <summary>
	/// Interaction logic for NewUserView.xaml
	/// </summary>
	public partial class NewUserView : Window
	{
		private UsersViewModel usersViewModel;

		public NewUserView()
		{
			InitializeComponent();
		}

		public NewUserView(UsersViewModel usersViewModel)
		{
			InitializeComponent();

			this.usersViewModel = usersViewModel;
		}
	}
}
