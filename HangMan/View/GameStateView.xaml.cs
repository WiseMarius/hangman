﻿using HangMan.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HangMan.View
{
	/// <summary>
	/// Interaction logic for GameView.xaml
	/// </summary>
	public partial class GameView : Window
	{
		private GameStateViewModel gameStateViewModel;
		private MainWindow mainWindow;
		private bool isGameLoaded;

		public GameView(MainWindow mainWindow, GameStateViewModel gameStateViewModel, bool pIsGameLoaded)
		{
			this.mainWindow = mainWindow;
			this.gameStateViewModel = gameStateViewModel;
			isGameLoaded = pIsGameLoaded;

			InitializeComponent();
		}
	}
}
