﻿using HangMan.Model;
using HangMan.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup;
using System.Xml.Serialization;

namespace HangMan
{
	public class SerializationManager
	{
		public static void Serialize(UsersModel users)
		{
			IFormatter formatter = new BinaryFormatter();
			Stream stream = new FileStream("users.bin", FileMode.Create, FileAccess.Write, FileShare.None);
			formatter.Serialize(stream, users);
			stream.Close();
		}

		public static void Serialize(StatsManagerModel stats)
		{
			IFormatter formatter = new BinaryFormatter();
			Stream stream = new FileStream("stats.bin", FileMode.Create, FileAccess.Write, FileShare.None);
			formatter.Serialize(stream, stats);
			stream.Close();
		}

		public static UsersModel DeserializeUsers()
		{
			IFormatter formatter = new BinaryFormatter();
			UsersModel obj = null;

			if (File.Exists("users.bin"))
			{
				Stream stream = new FileStream("users.bin", FileMode.Open, FileAccess.Read, FileShare.Read);
				obj = (UsersModel)formatter.Deserialize(stream);
				stream.Close();

				return obj;
			}

			return new UsersModel();
		}

		public static StatsManagerModel DeserializeStats()
		{
			IFormatter formatter = new BinaryFormatter();
			StatsManagerModel obj = null;

			if (File.Exists("stats.bin"))
			{
				Stream stream = new FileStream("stats.bin", FileMode.Open, FileAccess.Read, FileShare.Read);
				obj = (StatsManagerModel)formatter.Deserialize(stream);
				stream.Close();

				return obj;
			}

			return new StatsManagerModel();
		}

		public static void SerializeGameState(GameStateViewModel gameStateViewModel ,string filename)
		{
			XmlSerializer ser = new XmlSerializer(typeof(GameStateViewModel));
			TextWriter writer = new StreamWriter(filename);

			ser.Serialize(writer, gameStateViewModel);
			writer.Close();
		}

		public static GameStateViewModel DeserializeGameState(string filename)
		{
			XmlSerializer ser = new XmlSerializer(typeof(GameStateViewModel));
			FileStream fs = new FileStream(filename, FileMode.Open);
			GameStateViewModel gameStateViewModel = (GameStateViewModel)ser.Deserialize(fs);

			return gameStateViewModel;
		}
	}
}
