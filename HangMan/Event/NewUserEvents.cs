﻿using HangMan.Model;
using HangMan.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace HangMan.View
{
	public partial class NewUserView : Window
	{
		private void buttonSubmit_Click(object sender, RoutedEventArgs e)
		{
			if (textBoxUsername.Text == "" || usersViewModel.users.userExists(textBoxUsername.Text))
			{
				MessageBox.Show("Please chose a valid username");
			}
			else
			{
				UserModel user = new UserModel(textBoxUsername.Text);
				usersViewModel.users.Add(user);

				this.Close();
			}
		}
	}
}
