﻿using HangMan.Model;
using HangMan.View;
using HangMan.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace HangMan
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private void mainWindow_closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (MessageBox.Show("Close Application ?", "Question", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
			{
				SerializationManager.Serialize(usersViewModel.users);
			}
			else
			{
				e.Cancel = true;
			}
		}

		private void usersListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (usersList.SelectedIndex >= 0)
			{
				var currentUser = usersViewModel.users.ElementAt(usersList.SelectedIndex);

				usersViewModel.selectedUser = currentUser;

				deleteUserButton.IsEnabled = true;
				playButton.IsEnabled = true;
				nextButton.IsEnabled = true;
				previousButton.IsEnabled = true;

				profilePicturesViewModel.currentPicture = profilePicturesViewModel.profilePcitures.pictures.ElementAt(currentUser.avatar);
			}
		}

		private void deleteUserButton_Click(object sender, RoutedEventArgs e)
		{
			if (MessageBox.Show("Do you want to delete this user ?", "Question", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
			{
				int indexToRemove = usersList.SelectedIndex;
				string username = usersViewModel.users.ElementAt(indexToRemove).name;

				usersViewModel.users.deleteUser(username);
				statsManagerViewModel.statsManagerModel.removeUser(username);
				statsManagerViewModel.statsManagerModel.Serialize();

				usersList.SelectedIndex = 0;

				string saveGamePath = username + ".bin";

				if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + saveGamePath))
				{
					File.Delete(AppDomain.CurrentDomain.BaseDirectory + saveGamePath);
				}
			}
		}

		private void playButton_Click(object sender, RoutedEventArgs e)
		{
			this.Hide();

			var playMenu = new PlayMenuView(this);
			playMenu.Show();
		}

		private void cancelButton_Click(object sender, RoutedEventArgs e)
		{
			this.Close();
		}

		private void newUserButton_Click(object sender, RoutedEventArgs e)
		{
			var newUserWindow = new NewUserView(usersViewModel);
			newUserWindow.ShowDialog();
		}

		private void previousButton_Click(object sender, RoutedEventArgs e)
		{
			profilePicturesViewModel.previousProfilePictureIndex();

			usersViewModel.users.ElementAt(usersList.SelectedIndex).avatar = profilePicturesViewModel.profilePcitures.index;
		}

		private void nextButton_Click(object sender, RoutedEventArgs e)
		{
			profilePicturesViewModel.nextProfilePictureIndex();

			usersViewModel.users.ElementAt(usersList.SelectedIndex).avatar = profilePicturesViewModel.profilePcitures.index;
		}

		private void mainWindow_initialized(object sender, EventArgs e)
		{
			usersViewModel.users = SerializationManager.DeserializeUsers();

			this.avatarImage.DataContext = profilePicturesViewModel;
			this.usersList.ItemsSource = usersViewModel.users;
		}

		private void mainWindow_deactivated(object sender, EventArgs e)
		{
			usersList.SelectedIndex = usersViewModel.users.Count;
		}
	}
}
