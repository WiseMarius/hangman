﻿using System;

namespace HangMan.Event
{
	public delegate void MyEventHandler(object source, MyEventArgs e);

	public class MyEventArgs : EventArgs
	{
		private string EventInfo;
		public MyEventArgs(string Text)
		{
			EventInfo = Text;
		}
		public string GetInfo()
		{
			return EventInfo;
		}
	}
}
