﻿using HangMan.Event;
using HangMan.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace HangMan.View
{
	public partial class GameView : Window
	{
		private void about_Click(object sender, RoutedEventArgs e)
		{
			MessageBox.Show("Demeter Attila Marius\nGrupa: 10LF251", "About", MessageBoxButton.OK, MessageBoxImage.Information);
		}

		private void menuStart_Click(object sender, RoutedEventArgs e)
		{
			menuSave.IsEnabled = true;
			menuStart.IsEnabled = false;
			domainsTab.IsEnabled = false;

			startGame();
		}

		private void startGame()
		{
			gameStateViewModel.gameStateModel.timerViewModel.resetTimer();
			gameStateViewModel.start();

			gameStateViewModel.gameStateModel.wordViewModel = new WordViewModel(gameStateViewModel.gameStateModel.domainsViewModel.selectedIndex);
			initHandlers();

			this.wordMeesage.DataContext = gameStateViewModel.gameStateModel.wordViewModel;
			this.image.DataContext = gameStateViewModel.gameStateModel.stateImageViewModel;

			resetWrongLetter();
			resetLetters();
		}

		private void menuSave_Click(object sender, RoutedEventArgs e)
		{
			string fileName = mainWindow.usersViewModel.selectedUser.name + ".bin";

			gameStateViewModel.gameStateModel.isGameSaved = true;

			gameStateViewModel.stop();
			SerializationManager.SerializeGameState(gameStateViewModel, fileName);
			this.Close();
		}

		private void menuExit_Click(object sender, RoutedEventArgs e)
		{
			this.Close();
		}

		private void gameView_Closed(object sender, EventArgs e)
		{
			mainWindow.Show();
		}

		private void gameView_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			gameStateViewModel.stop();

			if (MessageBox.Show("Do you want to quit ?", "Question", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
			{
				e.Cancel = true;

				gameStateViewModel.resume();
			}
			else
			{
				mainWindow.statsManagerViewModel.statsManagerModel.Serialize();
			}
		}

		private void wrongLetter(object source, MyEventArgs e)
		{
			if (gameStateViewModel.gameActive)
			{
				gameStateViewModel.gameStateModel.stateImageViewModel.nextState();
			}
		}

		private void wordGuessed(object source, MyEventArgs e)
		{
			gameStateViewModel.stop();

			if (MessageBox.Show("You guessed it !", "Level up", MessageBoxButton.OK, MessageBoxImage.None) == MessageBoxResult.OK)
			{
				gameStateViewModel.nextRound();
				gameStateViewModel.gameStateModel.stateImageViewModel.resetState();

				resetWrongLetter();
				resetLetters();

				startGame();
			}
		}

		public void roundFinished(object source, MyEventArgs e)
		{
			if (gameStateViewModel.gameActive)
			{
				mainWindow.statsManagerViewModel.statsManagerModel.logGame(mainWindow.usersViewModel.selectedUser.name, gameStateViewModel.gameStateModel.domainsViewModel.selectedDomain, false);
				gameStateViewModel.stop();

				if (MessageBox.Show("You lost! Try again ?", "Question", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
				{
					restart();
				}
				else
				{
					menuStart.IsEnabled = true;
					domainsTab.IsEnabled = true;
				}
			}
		}

		private void restart()
		{
			gameStateViewModel.restart();
			gameStateViewModel.gameStateModel.stateImageViewModel.resetState();

			resetWrongLetter();
			resetLetters();

			startGame();
		}

		private void gameFinished(object source, MyEventArgs e)
		{
			mainWindow.statsManagerViewModel.statsManagerModel.logGame(mainWindow.usersViewModel.selectedUser.name, gameStateViewModel.gameStateModel.domainsViewModel.selectedDomain, true);

			MessageBox.Show("You won!", "Victory", MessageBoxButton.OK, MessageBoxImage.Information);
			restart();
		}

		private void checkButton(Button button)
		{
			if (gameStateViewModel.gameActive)
			{
				button.IsEnabled = false;
				gameStateViewModel.triedLetters += button.Content as string;

				if (!gameStateViewModel.gameStateModel.wordViewModel.addLetter(button.Content.ToString()))
				{
					markWrongLetter(button.Content.ToString());

					if (gameStateViewModel.buttonsWrongLetters[6].Content as string != "")
					{
						roundFinished(this, null);
					}
				}
			}
			else
			{
				MessageBox.Show("Game inactive", "HangMan", MessageBoxButton.OK, MessageBoxImage.Information);
			}
		}

		private void markWrongLetter(string letter)
		{
			for (int index = 0; index < 7; index++)
			{
				if (gameStateViewModel.buttonsWrongLetters[index].Content.ToString() == "")
				{
					gameStateViewModel.buttonsWrongLetters[index].Content = letter;
					gameStateViewModel.wrongLetters += letter;

					return;
				}
			}
		}

		private void resetLetters()
		{
			for (int index = 0; index < 26; index++)
			{
				gameStateViewModel.buttonsLetters[index].IsEnabled = true;
			}

			gameStateViewModel.triedLetters = "";
		}

		private void resetWrongLetter()
		{
			for (int index = 0; index < 7; index++)
			{
				gameStateViewModel.buttonsWrongLetters[index].Content = "";
			}

			gameStateViewModel.wrongLetters = "";
		}

		private void domainsContries_Click(object sender, RoutedEventArgs e)
		{
			initDomain(0);
		}

		private void domainsAnimals_Click(object sender, RoutedEventArgs e)
		{
			initDomain(1);
		}

		private void domainsFruits_Click(object sender, RoutedEventArgs e)
		{
			initDomain(2);
		}

		private void domainsSelectAll_Click(object sender, RoutedEventArgs e)
		{
			initDomain(3);
		}

		private void initDomain(byte index)
		{
			menuStart.IsEnabled = true;

			gameStateViewModel.gameStateModel.domainsViewModel.selectedIndex = index;
		}

		private void gameView_Initialized(object sender, EventArgs e)
		{
			this.welcomeMessage.Text = "Hi " + mainWindow.usersViewModel.selectedUser.name + " !";
			this.domainMessage.DataContext = gameStateViewModel.gameStateModel.domainsViewModel;
			this.timeLeft.DataContext = gameStateViewModel.gameStateModel.timerViewModel;
			this.currentRound.DataContext = gameStateViewModel;
			this.image.DataContext = gameStateViewModel.gameStateModel.stateImageViewModel;
			this.wordMeesage.DataContext = gameStateViewModel.gameStateModel.wordViewModel;

			gameStateViewModel.buttonsWrongLetters[0] = guessLetter1;
			gameStateViewModel.buttonsWrongLetters[1] = guessLetter2;
			gameStateViewModel.buttonsWrongLetters[2] = guessLetter3;
			gameStateViewModel.buttonsWrongLetters[3] = guessLetter4;
			gameStateViewModel.buttonsWrongLetters[4] = guessLetter5;
			gameStateViewModel.buttonsWrongLetters[5] = guessLetter6;
			gameStateViewModel.buttonsWrongLetters[6] = guessLetter7;

			gameStateViewModel.buttonsLetters[0] = letterA;
			gameStateViewModel.buttonsLetters[1] = letterB;
			gameStateViewModel.buttonsLetters[2] = letterC;
			gameStateViewModel.buttonsLetters[3] = letterD;
			gameStateViewModel.buttonsLetters[4] = letterE;
			gameStateViewModel.buttonsLetters[5] = letterF;
			gameStateViewModel.buttonsLetters[6] = letterG;
			gameStateViewModel.buttonsLetters[7] = letterH;
			gameStateViewModel.buttonsLetters[8] = letterI;
			gameStateViewModel.buttonsLetters[9] = letterJ;
			gameStateViewModel.buttonsLetters[10] = letterK;
			gameStateViewModel.buttonsLetters[11] = letterL;
			gameStateViewModel.buttonsLetters[12] = letterM;
			gameStateViewModel.buttonsLetters[13] = letterN;
			gameStateViewModel.buttonsLetters[14] = letterO;
			gameStateViewModel.buttonsLetters[15] = letterP;
			gameStateViewModel.buttonsLetters[16] = letterQ;
			gameStateViewModel.buttonsLetters[17] = letterR;
			gameStateViewModel.buttonsLetters[18] = letterS;
			gameStateViewModel.buttonsLetters[19] = letterT;
			gameStateViewModel.buttonsLetters[20] = letterU;
			gameStateViewModel.buttonsLetters[21] = letterV;
			gameStateViewModel.buttonsLetters[22] = letterW;
			gameStateViewModel.buttonsLetters[23] = letterX;
			gameStateViewModel.buttonsLetters[24] = letterY;
			gameStateViewModel.buttonsLetters[25] = letterZ;

			if (isGameLoaded)
			{
				resumeGame();
			}
		}

		private void resumeGame()
		{
			initHandlers();

			domainsTab.IsEnabled = false;
			menuSave.IsEnabled = true;

			foreach(char c in gameStateViewModel.wrongLetters)
			{
				markWrongLetter(c.ToString());
			}

			for (int index = 0; index < 26; index++)
			{
				if (gameStateViewModel.triedLetters.IndexOf(gameStateViewModel.buttonsLetters[index].Content as string, StringComparison.OrdinalIgnoreCase) >= 0)
				{
					gameStateViewModel.buttonsLetters[index].IsEnabled = false;
				}
			}
		}

		private void initHandlers()
		{
			gameStateViewModel.gameStateModel.timerViewModel.OnTimeOut -= new MyEventHandler(roundFinished);
			gameStateViewModel.gameStateModel.wordViewModel.OnWordGuessed -= new MyEventHandler(wordGuessed);
			gameStateViewModel.gameStateModel.wordViewModel.OnWrongLetter -= new MyEventHandler(wrongLetter);
			gameStateViewModel.OnGameWon -= new MyEventHandler(gameFinished);

			gameStateViewModel.gameStateModel.timerViewModel.OnTimeOut += new MyEventHandler(roundFinished);
			gameStateViewModel.gameStateModel.wordViewModel.OnWordGuessed += new MyEventHandler(wordGuessed);
			gameStateViewModel.gameStateModel.wordViewModel.OnWrongLetter += new MyEventHandler(wrongLetter);
			gameStateViewModel.OnGameWon += new MyEventHandler(gameFinished);
		}

		private void letter_Click(object sender, RoutedEventArgs e)
		{
			checkButton(sender as Button);
		}
	}
}
