﻿using HangMan.Event;
using HangMan.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace HangMan.View
{
	public partial class PlayMenuView : Window
	{
		private void buttonNewGame_Click(object sender, RoutedEventArgs e)
		{
			newGamePressed = true;
			this.Close();

			var game = new GameView(mainWindow, gameState, false);
			game.Show();
		}

		private void buttonOpenGame_Click(object sender, RoutedEventArgs e)
		{
			var gameStateViewModel = SerializationManager.DeserializeGameState(saveGamePath);
			newGamePressed = true;
			this.Close();

			var game = new GameView(mainWindow, gameStateViewModel, true);
			game.Show();
			gameStateViewModel.resume();
		}

		private void buttonStatistics_Click(object sender, RoutedEventArgs e)
		{
			var stats = new StatsView();

			stats.ShowDialog();
		}

		private void buttonBack_Click(object sender, RoutedEventArgs e)
		{
			this.Close();
		}

		private void playMenu_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!newGamePressed)
			{
				mainWindow.Show();

				newGamePressed = false;
			}
		}

		private void playMenu_Initialized(object sender, EventArgs e)
		{
			saveGamePath = mainWindow.usersViewModel.selectedUser.name + ".bin";

			if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + saveGamePath))
			{
				buttonOpenGame.IsEnabled = true;
			}
		}
	}
}
